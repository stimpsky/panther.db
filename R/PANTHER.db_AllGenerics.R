
setGeneric(".keytypes_core", function(x) standardGeneric(".keytypes_core"))
setGeneric(".type2table", function(x) standardGeneric(".type2table"))
setGeneric(".type2col", function(x) standardGeneric(".type2col"))
setGeneric("getTableNames", function(x) standardGeneric("getTableNames"))
setGeneric("availablePantherSpecies", function(x) standardGeneric("availablePantherSpecies"))
setGeneric("resetSpecies", function(x) standardGeneric("resetSpecies"))
setGeneric("species<-", function(x, value) standardGeneric("species<-"))
setGeneric("traverseClassTree", function(x,query,scope) standardGeneric("traverseClassTree"))


